.PHONY: all
all: \
	bin/ntx_partitions_parser \
	bin/extract-partitions.sh

bin/ntx_partitions_parser: src/ntx_partitions_parser.c src/common.c
	install -d bin
	$(CC) $^ -o $@

bin/test: src/test.c src/common.c
	install -d bin
	$(CC) $^ -o $@

bin/extract-partitions.sh: src/extract-partitions.sh
	install -d bin
	install -t bin -m 755 $<

.PHONY: test
test: bin/test
	bin/test

clean:
	rm -f \
		bin/extract-partitions.sh \
		bin/ntx_partitions_parser
	rmdir bin/
