#ifndef NTX_FW_H
#define NTX_FW_H

#define NTX_FIRMWARE_SIGNATURE_SIZE	10
#define NTX_FIRMWARE_NAME_LEN		32
#define NTX_FW_ITEM_MAGIC_SIZE		8

enum ntx_fw_type {
	/* frontlight percentage table for lm3630 */
	NTX_FW_TYPE_LM3630_FLPERCTTAB,

	/* frontlight current table for lm3630/ricoh */
	NTX_FW_TYPE_LM3630_FLCURTABLE,

	/* frontlight current table header for lm3630/ricoh
	 * for each combinations on 4colors(R/G/B/W) models
	 */
	NTX_FW_TYPE_LM3630_RGBW_CURTAB_HDR,

	/* frontlight current table for lm3630/ricoh
	 * for each combinations on 4colors(R/G/B/W) models
	 */
	NTX_FW_TYPE_LM3630_RGBW_CURTAB,

	/* frontlight current table for lm3630/ricoh
	 * for each combinations on 2colors mix to 11 colors
	 * 100 percent current tables
	 */
	NTX_FW_TYPE_LM3630_MIX2COLOR11_CURTAB,

	/* frontlight 100% current table for ricoh */
	NTX_FW_TYPE_FLPERCETCURTABLE,

	/* frontlight table header for lm3630
	 * for each 100% table which combine 2colors
	 */
	NTX_FW_TYPE_LM3630_DUALFL_HDR,

	/* frontlight percent table for lm3630
	 * for each combinations on 2colors models
	 */
	NTX_FW_TYPE_LM3630_DUALFL_PERCENTTAB,
};

char* ntx_firmware_type_name[] = {
	"NTX_FW_TYPE_LM3630_FLPERCTTAB",
	"NTX_FW_TYPE_LM3630_FLCURTABLE",
	"NTX_FW_TYPE_LM3630_RGBW_CURTAB_HDR",
	"NTX_FW_TYPE_LM3630_RGBW_CURTAB",
	"NTX_FW_TYPE_LM3630_MIX2COLOR11_CURTAB",
	"NTX_FW_TYPE_FLPERCETCURTABLE",
	"NTX_FW_TYPE_LM3630_DUALFL_HDR",
	"NTX_FW_TYPE_LM3630_DUALFL_PERCENTTAB",
};

struct NTX_FIRMWARE_HDR {
	uint8_t szSignatureS[NTX_FIRMWARE_SIGNATURE_SIZE];
	uint8_t szFirmwareName[NTX_FIRMWARE_NAME_LEN+4];
	uint16_t wFirmwareItems;
	uint8_t bVersionMajor;
	uint8_t bVersionSub;
	uint8_t bVersionMinor;
	uint8_t bVersionMini;
};

struct NTX_FIRMWARE_ITEM_HDR {
	uint8_t bMagicA[NTX_FW_ITEM_MAGIC_SIZE];
	uint32_t dw12345678;
	uint16_t wFirmwareType;
	uint16_t wReserved;
	uint32_t dwFirmwareSize;
	uint8_t szFirmwareName[NTX_FIRMWARE_NAME_LEN+4];
};

#endif /* NTX_FW_H */
