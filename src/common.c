/*
 * Copyright (C) 2019 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"

int get_hex_string(char** str_p, char* buffer, size_t buffer_size)
{
	int buffer_pos = 0;
	int str_pos = 0;
	int group_elements = 4;
	int nr_spaces;
	size_t str_size;
	char byte[3];
	char* str;

	/* Format: ABCDEF12 34 */
	nr_spaces = ((2*buffer_size) / group_elements) -1 ;
	str_size = (2*buffer_size) + nr_spaces + 1;
	str = calloc(1, str_size);
	CHECK_FOR_ERRNO(str, NULL, "calloc");
	*str_p = str;

	dbg_printf("buffer size: %d str_size: %d\n", buffer_size, str_size);
	/* -1 for the string termination, we also do -2 right after */
	str_pos = str_size -1;

	for(buffer_pos=0; buffer_pos<buffer_size;) {
		str_pos -= 2;

		snprintf(byte, 2+1, "%02hhX", (buffer[buffer_pos] & 0xff));
		memcpy(str + str_pos, byte, 2);

		dbg_printf("[@%d] str[%d:%d]: 0x%s\n", buffer_pos, str_pos,
			   str_pos+1, byte);

		buffer_pos++;

		/* Add padding space */
		if ((nr_spaces > 0 && (buffer_pos > 0)) && (str_pos > 0)) {
			if ((buffer_pos % (group_elements/2)) == 0) {
				char space = ' ';
				str_pos--;
				memcpy(str + str_pos, &space, 1);
				dbg_printf("[@%d] SPACE[%d] !!!\n", buffer_pos, str_pos);
			}
		}
	}
	dbg_printf("%s: string@%p: %s\n", __FUNCTION__, str, str);
	dbg_printf("result: '%s'\n", str);
}
