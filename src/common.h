#ifndef NTX_PARTITIONS_PARSER_COMMON_H
#define NTX_PARTITIONS_PARSER_COMMON_H

#define DEBUG 0
#if DEBUG
#define dbg_printf printf
#else
#define dbg_printf while(0)
#endif

#define CHECK_FOR_ERRNO(ret, error_value, name)				\
	if(ret == error_value) {					\
		int err = errno;					\
		printf("%s error: %s\n", name, strerror(err));		\
		exit(err);						\
	} while(0)

int get_hex_string(char** str_p, char* buffer, size_t buffer_size);

#endif /* NTX_PARTITIONS_PARSER_COMMON_H */
