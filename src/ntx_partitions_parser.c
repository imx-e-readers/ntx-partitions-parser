/*
 * Copyright (C) 2019 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>

#include "hwcfg.h"
#include "ntx_fw.h"
#include "ntx_fw_lm3630fl.h"

#include "common.h"

static int print_ntxfw_header(void* file, size_t file_size, size_t offset)
{
	struct NTX_FIRMWARE_HDR* header = (struct NTX_FIRMWARE_HDR*)(file + offset);
	char* signature;
	char* firmware_name;
	int ret;

	signature = calloc(1, NTX_FIRMWARE_SIGNATURE_SIZE + 1);
	CHECK_FOR_ERRNO(signature, NULL, "calloc");
	memcpy(signature, header->szSignatureS, NTX_FIRMWARE_SIGNATURE_SIZE);

	firmware_name = calloc(1, NTX_FIRMWARE_NAME_LEN + 4 + 1);
	CHECK_FOR_ERRNO(firmware_name, NULL, "calloc");
	memcpy(firmware_name, header->szFirmwareName, NTX_FIRMWARE_NAME_LEN + 4);

	printf("NTX_FIRMWARE_HDR [start:0x%x|len:%d|end:0x%x] {\n"
	       "\tszSignatureS: '%s',\n"
	       "\tszFirmwareName: '%s',\n"
	       "\twFirmwareItems: %d,\n"
	       "\tbVersionMajor: %d,\n"
	       "\tbVersionSub: %d,\n"
	       "\tbVersionMinor: %d,\n"
	       "\tbVersionMini: %d,\n"
	       "};\n",
	       offset,
	       sizeof(struct NTX_FIRMWARE_HDR),
	       offset + sizeof(struct NTX_FIRMWARE_HDR) -1,
	       signature,
	       firmware_name,
	       header->wFirmwareItems,
	       header->bVersionMajor,
	       header->bVersionSub,
	       header->bVersionMinor,
	       header->bVersionMini);

	free(signature);
	free(firmware_name);

	return 0;
}

static void get_ntxfw_section_firmware_type(char** str_p,
					   enum ntx_fw_type firmware_type)
{
	if ( (firmware_type < 0) ||
	     (firmware_type > NTX_FW_TYPE_LM3630_DUALFL_PERCENTTAB) ) {
		/* Don't try to access the table as the firmware_type
		 * index is controlled by the binary
		 */
		char* str = calloc(1, (2*2) + 1);
		CHECK_FOR_ERRNO(str, NULL, "calloc");
		snprintf(str, 2*2, "%0hX", firmware_type);
		*str_p = str;
	} else {
		size_t hex_size = sizeof("0x") + (2*2) + 1;
		size_t str_size = hex_size + 1 + 2 +
			strlen(ntx_firmware_type_name[firmware_type]) + 1;

		char* str = calloc(1, str_size);
		CHECK_FOR_ERRNO(str, NULL, "calloc");
		snprintf(str, str_size - 1, "0x%0hX (%s)",
			 firmware_type,
			 ntx_firmware_type_name[firmware_type]);
		*str_p = str;
	}
}

static int print_ntxfw_lm3630_dualfl_header(void* file, size_t file_size,
					    size_t offset)
{
	struct NTX_FW_LM3630FL_dualcolor_hdr* header = (file + offset);

	printf("NTX_FW_LM3630FL_dualcolor_hdr [start:0x%x|len:%d|end:0x%x] {\n"
	       "\tdwTotalColors: %d,\n"
	       "\tbDefaultC1_Current: %d,\n"
	       "\tbDefaultC2_Current: %d,\n"
	       "\tbDefaultFreq: %d,\n"
	       "\tbDefaultFlags: %d,\n"
	       "};\n",
	       offset,
	       sizeof(struct NTX_FW_LM3630FL_dualcolor_hdr),
	       offset + sizeof(struct NTX_FW_LM3630FL_dualcolor_hdr) -1,
	       header->dwTotalColors,
	       header->bDefaultC1_Current,
	       header->bDefaultC2_Current,
	       header->bDefaultFreq,
	       header->bDefaultFlags);
}


static int print_ntxfw_lm3630_dualfl_percent_tab(void* file, size_t file_size,
						 size_t offset)
{
	struct NTX_FW_LM3630FL_dualcolor_percent_tab* header = (file + offset);
	char* sz_color_name;
	char* b_c1_brightness_a;
	char* b_c1_current_a;
	char* b_c2_brightness_a;
	char* b_c2_current_a;
	char* d_wcurrent_a;
	char* b_reserved_ap;

	get_hex_string(&sz_color_name, header->szColorName, 32);
	get_hex_string(&b_c1_brightness_a, header->bC1_BrightnessA, 100);
	get_hex_string(&b_c1_current_a, header->bC1_CurrentA, 100);
	get_hex_string(&b_c2_brightness_a, header->bC2_BrightnessA, 100);
	get_hex_string(&b_c2_current_a, header->bC2_CurrentA, 100);
	get_hex_string(&d_wcurrent_a, (char*)header->dwCurrentA, 100);
	get_hex_string(&b_reserved_ap, header->bReservedA, 8);

	printf("NTX_FW_LM3630FL_dualcolor_percent_tab [start:0x%x|len:%d|end:0x%x] {\n"
	       "\tszColorName: '%s',\n"
	       "\tbDefaultC1_Current: %d\n"
	       "\tbDefaultC2_Current: %d\n"
	       "\tbDefaultFreq: %d\n"
	       "\tbDefaultFlags: %d\n"
	       "\tbC1_BrightnessA: 0x%s\n"
	       "\tbC1_CurrentA: 0x%s\n"
	       "\tbC2_BrightnessA: 0x%s\n"
	       "\tbC2_CurrentA: 0x%s\n"
	       "\tdwCurrentA: 0x%s\n"
	       "\tbReservedA: 0x%s\n"
	       "};\n",
	       offset,
	       sizeof(struct NTX_FW_LM3630FL_dualcolor_percent_tab),
	       offset + sizeof(struct NTX_FW_LM3630FL_dualcolor_percent_tab) -1,
	       sz_color_name,
	       header->bDefaultC1_Current,
	       header->bDefaultC2_Current,
	       header->bDefaultFreq,
	       header->bDefaultFlags,
	       b_c1_brightness_a,
	       b_c1_current_a,
	       b_c2_brightness_a,
	       b_c2_current_a,
	       d_wcurrent_a,
	       b_reserved_ap);

	free(sz_color_name);
	free(b_c1_brightness_a);
	free(b_c1_current_a);
	free(b_c2_brightness_a);
	free(b_c2_current_a);
	free(d_wcurrent_a);
	free(b_reserved_ap);

	printf("NTX_FW_LM3630FL_dualcolor_percent_tab:\n"
	       "\tposition after the data structure: 0x%x\n"
	       "\tremaining unknown data: %d\n",
	       offset + sizeof(struct NTX_FW_LM3630FL_dualcolor_percent_tab),
	       file_size - offset - sizeof(struct NTX_FW_LM3630FL_dualcolor_percent_tab));
}

static int print_ntxfw_single_section_data(void* file, size_t file_size,
					   size_t offset,
					   enum ntx_fw_type firmware_type)
{
	switch (firmware_type) {
	case NTX_FW_TYPE_LM3630_DUALFL_HDR:
		print_ntxfw_lm3630_dualfl_header(file, file_size, offset);
		break;
	case NTX_FW_TYPE_LM3630_DUALFL_PERCENTTAB:
		print_ntxfw_lm3630_dualfl_percent_tab(file, file_size, offset);
		break;
	default:
		printf("TODO: implement firmware_type %d\n", firmware_type);
	}
}

static int print_ntxfw_single_section_header(void* file, size_t file_size,
					     size_t offset, unsigned int section)
{
	struct NTX_FIRMWARE_ITEM_HDR* header =
		(struct NTX_FIRMWARE_ITEM_HDR*)(file + offset);

	char* b_magic_a;
	char* firmware_name;
	char* firmware_type;

	int ret;

	get_hex_string(&b_magic_a, header->bMagicA, NTX_FW_ITEM_MAGIC_SIZE);

	firmware_name = calloc(1, NTX_FIRMWARE_NAME_LEN + 4 + 1);
	CHECK_FOR_ERRNO(firmware_name, NULL, "calloc");
	memcpy(firmware_name, header->szFirmwareName, NTX_FIRMWARE_NAME_LEN + 4);

	get_ntxfw_section_firmware_type(&firmware_type, header->wFirmwareType);

	printf("NTX_FIRMWARE_ITEM_HDR[%d] [start:0x%x|len:%d|end:0x%x] {\n"
	       "\t bMagicA: 0x%s,\n"
	       "\t dw12345678: 0x%x (%s),\n"
	       "\t wFirmwareType: %s,\n"
	       "\t wReserved: 0x%x,\n"
	       "\t dwFirmwareSize: %d,\n"
	       "\t szFirmwareName: '%s',\n"
	       "};\n",
	       section,
	       offset,
	       sizeof(struct NTX_FIRMWARE_ITEM_HDR),
	       offset + sizeof(struct NTX_FIRMWARE_ITEM_HDR) -1,
	       b_magic_a,
	       header->dw12345678,
	       (header->dw12345678 == 0x12345678) ?
	       "0x12345678: Signature OK" : "Signature Failed",
	       firmware_type,
	       header->wReserved,
	       header->dwFirmwareSize,
	       firmware_name);

	free(b_magic_a);

	print_ntxfw_single_section_data(
		file,
		header->dwFirmwareSize,
		offset + sizeof(struct NTX_FIRMWARE_ITEM_HDR),
		header->wFirmwareType);

	return 0;
}

static int print_ntxfw_all_sections_headers(void* file, size_t file_size, size_t offset)
{
	struct NTX_FIRMWARE_HDR* main_header;
	struct NTX_FIRMWARE_ITEM_HDR* section_header;
	size_t section_header_offset = 0;
	char* end_signature;
	int nr_sections;
	int section;

	main_header = (struct NTX_FIRMWARE_HDR*)(file + offset);
	nr_sections = main_header->wFirmwareItems;
	section_header_offset = offset + sizeof(struct NTX_FIRMWARE_HDR);

	printf("Found %d NTX_FIRMWARE_ITEMs:\n",nr_sections);
	for (section=0; section<nr_sections; section++) {
		print_ntxfw_single_section_header(file, file_size, section_header_offset, section);
		section_header = file + section_header_offset;
		section_header_offset += sizeof(struct NTX_FIRMWARE_ITEM_HDR);
		section_header_offset += section_header->dwFirmwareSize;
	}

	/*
	 * End signature
	 */
	end_signature = calloc(1, NTX_FIRMWARE_SIGNATURE_SIZE + 1);
	CHECK_FOR_ERRNO(end_signature, NULL, "calloc");

	memcpy(end_signature, file + section_header_offset,
	       NTX_FIRMWARE_SIGNATURE_SIZE);

	printf("NTX_FIRMWARE End signature [start:0x%x|len:%d|end:0x%x]: '%s'\n",
	       section_header_offset,
	       NTX_FIRMWARE_SIGNATURE_SIZE,
	       section_header_offset + NTX_FIRMWARE_SIGNATURE_SIZE -1,
	       end_signature);

	/*
	 * End of analyzed file
	 */
	section_header_offset += NTX_FIRMWARE_SIGNATURE_SIZE;

	printf("ntxfw:\n"
	       "\tsize:%d\n"
	       "\tposition after the last firmware item: 0x%x\n"
	       "\tunanalyzed data size: %d\n"
	       "\n",
	       file_size, section_header_offset,
	       file_size - section_header_offset);
}

static int print_hwcfg_header(void* file, size_t file_size, size_t offset)
{
	struct NTX_HWCONFIG* header = (struct NTX_HWCONFIG*)(file + offset);
	struct NTXHWCFG_HDR* m_hdr;
	struct NTXHWCFG_VAL* m_val;
	char* c_magic_name_a;
	char* c_version_name_a;

	m_hdr = &(header->m_hdr);
	m_val = &(header->m_val);

	c_magic_name_a = calloc(1, 10 + 1);
	CHECK_FOR_ERRNO(c_magic_name_a, NULL, "calloc");
	memcpy(c_magic_name_a, m_hdr->cMagicNameA, 10);

	c_version_name_a = calloc(1, 5 + 1);
	CHECK_FOR_ERRNO(c_version_name_a, NULL, "calloc");
	memcpy(c_version_name_a, m_hdr->cVersionNameA, 5);

	printf("NTX_HWCONFIG {\n"
	       "\tNTXHWCFG_HDR{\n"
	       "\t\tcMagicNameA: '%s',\n"
	       "\t\tcVersionNameA: '%s',\n"
	       "\t\tbHWConfigSize: %d,\n"
	       "\t};\n"
	       "\tNTXHWCFG_VAL{\n"
	       "\t\tbPCB: %d,\n"
	       "\t\tbKeyPad: %d,\n"
	       "\t\tbAudioCodec: %d,\n"
	       "\t\tbAudioAmp: %d,\n"
	       "\t\tbWifi: %d,\n"
	       "\t\tbBT: %d,\n"
	       "\t\tbMobile: %d,\n"
	       "\t\tbTouchCtrl: %d,\n"
	       "\t\tbTouchType: %d,\n"
	       "\t\tbDisplayCtrl: %d,\n"
	       "\t\tbDisplayPanel: %d,\n"
	       "\t\tbRSensor: %d,\n"
	       "\t\tbMicroP: %d,\n"
	       "\t\tbCustomer: %d,\n"
	       "\t\tbBattery: %d,\n"
	       "\t\tbLed: %d,\n"
	       "\t\tbRamSize: %d,\n"
	       "\t\tbIFlash: %d,\n"
	       "\t\tbExternalMem: %d,\n"
	       "\t\tbRootFsType: %d,\n"
	       "\t\tbSysPartType: %d,\n"
	       "\t\tbProgressXHiByte: %d,\n"
	       "\t\tbProgressXLoByte: %d,\n"
	       "\t\tbProgressYHiByte: %d,\n"
	       "\t\tbProgressYLoByte: %d,\n"
	       "\t\tbProgressCnts: %d,\n"
	       "\t\tbContentType: %d,\n"
	       "\t\tbCPU: %d,\n"
	       "\t\tbUIStyle: %d,\n"
	       "\t\tbRamType: %d,\n"
	       "\t\tbUIConfig: %d,\n"
	       "\t\tbDisplayResolution: %d,\n"
	       "\t\tbFrontLight: %d,\n"
	       "\t\tbCPUFreq: %d,\n"
	       "\t\tbHallSensor: %d,\n"
	       "\t\tbDisplayBusWidth: %d,\n"
	       "\t\tbFrontLight_Flags: %d,\n"
	       "\t\tbPCB_Flags: %d,\n"
	       "\t\tbFrontLight_LED_Driver: %d,\n"
	       "\t\tbVCOM_10mV_HiByte: %d,\n"
	       "\t\tbVCOM_10mV_LoByte: %d,\n"
	       "\t\tbPCB_REV: %d,\n"
	       "\t\tbPCB_LVL: %d,\n"
	       "\t\tbHOME_LED_PWM: %d,\n"
	       "\t\tbPMIC: %d,\n"
	       "\t\tbFL_PWM: %d,\n"
	       "\t\tbRTC: %d,\n"
	       "\t\tbBootOpt: %d,\n"
	       "\t\tbTouch2Ctrl: %d,\n"
	       "\t\tbTouch2Type: %d,\n"
	       "\t\tbGPS: %d,\n"
	       "\t\tbFM: %d,\n"
	       "\t\tbRSensor2: %d,\n"
	       "\t\tbLightSensor: %d,\n"
	       "\t\tbTPFWIDByte0: %d,\n"
	       "\t\tbTPFWIDByte1: %d,\n"
	       "\t\tbTPFWIDByte2: %d,\n"
	       "\t\tbTPFWIDByte3: %d,\n"
	       "\t\tbTPFWIDByte4: %d,\n"
	       "\t\tbTPFWIDByte5: %d,\n"
	       "\t\tbTPFWIDByte6: %d,\n"
	       "\t\tbTPFWIDByte7: %d,\n"
	       "\t\tbGPU: %d,\n"
	       "\t};\n"
	       "};\n",
	       c_magic_name_a,
	       c_version_name_a,
	       m_hdr->bHWConfigSize,
	       m_val->bPCB,
	       m_val->bKeyPad,
	       m_val->bAudioCodec,
	       m_val->bAudioAmp,
	       m_val->bWifi,
	       m_val->bBT,
	       m_val->bMobile,
	       m_val->bTouchCtrl,
	       m_val->bTouchType,
	       m_val->bDisplayCtrl,
	       m_val->bDisplayPanel,
	       m_val->bRSensor,
	       m_val->bMicroP,
	       m_val->bCustomer,
	       m_val->bBattery,
	       m_val->bLed,
	       m_val->bRamSize,
	       m_val->bIFlash,
	       m_val->bExternalMem,
	       m_val->bRootFsType,
	       m_val->bSysPartType,
	       m_val->bProgressXHiByte,
	       m_val->bProgressXLoByte,
	       m_val->bProgressYHiByte,
	       m_val->bProgressYLoByte,
	       m_val->bProgressCnts,
	       m_val->bContentType,
	       m_val->bCPU,
	       m_val->bUIStyle,
	       m_val->bRamType,
	       m_val->bUIConfig,
	       m_val->bDisplayResolution,
	       m_val->bFrontLight,
	       m_val->bCPUFreq,
	       m_val->bHallSensor,
	       m_val->bDisplayBusWidth,
	       m_val->bFrontLight_Flags,
	       m_val->bPCB_Flags,
	       m_val->bFrontLight_LED_Driver,
	       m_val->bVCOM_10mV_HiByte,
	       m_val->bVCOM_10mV_LoByte,
	       m_val->bPCB_REV,
	       m_val->bPCB_LVL,
	       m_val->bHOME_LED_PWM,
	       m_val->bPMIC,
	       m_val->bFL_PWM,
	       m_val->bRTC,
	       m_val->bBootOpt,
	       m_val->bTouch2Ctrl,
	       m_val->bTouch2Type,
	       m_val->bGPS,
	       m_val->bFM,
	       m_val->bRSensor2,
	       m_val->bLightSensor,
	       m_val->bTPFWIDByte0,
	       m_val->bTPFWIDByte1,
	       m_val->bTPFWIDByte2,
	       m_val->bTPFWIDByte3,
	       m_val->bTPFWIDByte4,
	       m_val->bTPFWIDByte5,
	       m_val->bTPFWIDByte6,
	       m_val->bTPFWIDByte7,
	       m_val->bGPU);

	printf("hwcfg:\n"
	       "\tsize:%d\n"
	       "\tposition after the header: 0x%x\n"
	       "\tunanalyzed data size: %d\n"
	       "\n",
	       file_size, sizeof(struct NTX_HWCONFIG),
	       file_size - sizeof(struct NTX_HWCONFIG));

	return 0;
}

static int mmap_file(char* path, void** file, int* file_size)
{
	struct stat statbuf;
	int fd;
	int ret;

	fd = open(path, O_RDONLY);
	CHECK_FOR_ERRNO(fd, -1, "open");

	ret = fstat(fd, &statbuf);
	CHECK_FOR_ERRNO(fd, -1, "fstat");

	*file_size = statbuf.st_size;

	*file = mmap(NULL, statbuf.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	CHECK_FOR_ERRNO(*file, MAP_FAILED, "mmap");
}

void usage(char* progname)
{
	printf("%s [path/to/directory/with/extracted/emmc/partitions]\n", progname);
	exit (0);
}

int main(int argc, char** argv)
{
	void* ntxfw;
	int ntxfw_size;
	char* ntxfw_image_path;

	void* hwcfg;
	int hwcfg_size;
	char* hwcfg_image_path;

	char* basedir;

	if (argc != 2)
		usage(argv[0]);
	basedir = argv[1];

	ntxfw_image_path = calloc(1, strlen("/ntxfw.img") + strlen(basedir) +1);
	CHECK_FOR_ERRNO(ntxfw_image_path, NULL, "calloc");
	snprintf(ntxfw_image_path, strlen("/ntxfw.img") + strlen(basedir) +1,
		 "%s/ntxfw.img", basedir);
	mmap_file(ntxfw_image_path, &ntxfw, &ntxfw_size);

	print_ntxfw_header(ntxfw, ntxfw_size, 0);
	print_ntxfw_all_sections_headers(ntxfw, ntxfw_size, 0);

	hwcfg_image_path = calloc(1, strlen("/hwcfg.img") + strlen(basedir) +1);
	CHECK_FOR_ERRNO(hwcfg_image_path, NULL, "calloc");
	snprintf(hwcfg_image_path, strlen("/hwcfg.img") + strlen(basedir) +1,
		 "%s/hwcfg.img", basedir);
	mmap_file(hwcfg_image_path, &hwcfg, &hwcfg_size);
	print_hwcfg_header(hwcfg, hwcfg_size, 0);

	return 0;
}
