== Example usage ==
$ ./bin/extract-partitions.sh kobo_emmc.img
$ make
$ ./bin/ntx_partitions_parser partitions

== References ==
The partitions data structures come from the kernel of the kobo Aura H2O Edition 2
