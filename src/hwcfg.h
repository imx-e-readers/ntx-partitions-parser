#ifndef HW_CFG_H
#define HW_CFG_H

struct NTXHWCFG_VAL {
        unsigned char bPCB;//
        unsigned char bKeyPad;// key pad type .
        unsigned char bAudioCodec;//
        unsigned char bAudioAmp;//
        unsigned char bWifi;//
        unsigned char bBT;//
        unsigned char bMobile;//
        unsigned char bTouchCtrl;//touch controller .
        unsigned char bTouchType;//touch type .
        unsigned char bDisplayCtrl;//
        unsigned char bDisplayPanel;//
        unsigned char bRSensor;//
        unsigned char bMicroP;//
        unsigned char bCustomer;//
        unsigned char bBattery;//
        unsigned char bLed;//
        unsigned char bRamSize;// ram size
        unsigned char bIFlash; // internal flash type .
        unsigned char bExternalMem;// external sd type .
        unsigned char bRootFsType;// root fs type .
        unsigned char bSysPartType;// system partition type .
        unsigned char bProgressXHiByte; // Progress bar position while boot ,X
        unsigned char bProgressXLoByte; //                                  ,X
        unsigned char bProgressYHiByte; //                                                      ,Y
        unsigned char bProgressYLoByte; //                                                      ,Y
        unsigned char bProgressCnts;            //                                                      Cnt .
        unsigned char bContentType; // Book content type .
        unsigned char bCPU; //main cpu type .
        unsigned char bUIStyle; // UI style .
        unsigned char bRamType; // Ram Type .
        unsigned char bUIConfig; // UI config .
        unsigned char bDisplayResolution;// Display resolution .
        unsigned char bFrontLight;// Front Light .
        unsigned char bCPUFreq;// CPU frequency  .
        unsigned char bHallSensor;// Hall Sensor Controller  .
        unsigned char bDisplayBusWidth;// Display BUS width/bits .
        unsigned char bFrontLight_Flags;// Front Light Flags .
        unsigned char bPCB_Flags;// PCB Flags .
        unsigned char bFrontLight_LED_Driver;// Front Light LED driver .
        unsigned char bVCOM_10mV_HiByte;// VCOM mV High byte .
        unsigned char bVCOM_10mV_LoByte;// VCOM mV Low byte .
        unsigned char bPCB_REV;// PCB revision .
        unsigned char bPCB_LVL;// PCB develop level .
        unsigned char bHOME_LED_PWM;// HOME LED PWM source .
        unsigned char bPMIC;// System PMIC .
        unsigned char bFL_PWM;// FrontLight PWM source
        unsigned char bRTC;// System RTC source
        unsigned char bBootOpt;// Boot option
        unsigned char bTouch2Ctrl;//touch controller .
        unsigned char bTouch2Type;//touch type .
        unsigned char bGPS;//GPS module .
        unsigned char bFM;//FM module .
        unsigned char bRSensor2;//
        unsigned char bLightSensor;//
        unsigned char bTPFWIDByte0;// TP firmware ID byte0 .
        unsigned char bTPFWIDByte1;// TP firmware ID byte1 .
        unsigned char bTPFWIDByte2;// TP firmware ID byte2 .
        unsigned char bTPFWIDByte3;// TP firmware ID byte3 .
        unsigned char bTPFWIDByte4;// TP firmware ID byte4 .
        unsigned char bTPFWIDByte5;// TP firmware ID byte5 .
        unsigned char bTPFWIDByte6;// TP firmware ID byte6 .
        unsigned char bTPFWIDByte7;// TP firmware ID byte7 .
        unsigned char bGPU; //GPU .
};

struct NTXHWCFG_HDR {
        char cMagicNameA[10];// should be "HW CONFIG "
        char cVersionNameA[5];// should be "vx.x"
        unsigned char bHWConfigSize;// v0.1=19 .
};

struct NTX_HWCONFIG {
        struct NTXHWCFG_HDR m_hdr;
        struct NTXHWCFG_VAL m_val;
        unsigned char m_bReserveA[110-sizeof(struct NTXHWCFG_HDR)-sizeof(struct NTXHWCFG_VAL)];
};

#endif /* HW_CFG_H */
