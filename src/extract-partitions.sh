#!/bin/sh
# Copyright (C) 2019 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e
thisdir="$(dirname $(realpath $0))"
default_destdir="${thisdir}/../partitions"

usage()
{
	echo "$0 <path/to/emmc.img> [destdir]"
	exit 1
}

if [ $# -ne 1 -a $# -ne 2 ] ; then
	usage
fi

emmc_image="$1"
destdir="$2"
if [ "${destdir}" == "" ] ; then
    destdir="${default_destdir}"
fi

mkdir -p "${destdir}"

dd if="${emmc_image}" of="${destdir}/mbr.img" count=1
dd if="${emmc_image}" of="${destdir}/sn.img" skip=1 count=1
dd if="${emmc_image}" of="${destdir}/bootloader.img" skip=2 count=1022
dd if="${emmc_image}" of="${destdir}/hwcfg.img" skip=1024 count=2
dd if="${emmc_image}" of="${destdir}/ntxfw.img" skip=1030 count=255
dd if="${emmc_image}" of="${destdir}/waveform.img" skip=14336 count=20480
dd if="${emmc_image}" of="${destdir}/logo.img" skip=34816 count=4096
dd if="${emmc_image}" of="${destdir}/bootenv.img" skip=1536 count=510
dd if="${emmc_image}" of="${destdir}/kernel.img" skip=2048 count=12284
dd if="${emmc_image}" of="${destdir}/dtb.img" skip=1286 count=250
dd if="${emmc_image}" of="${destdir}/rootfs.img" skip=49152 count=524289
dd if="${emmc_image}" of="${destdir}/vfat.img" skip=1097730 count=14139389
dd if="${emmc_image}" of="${destdir}/recoveryfs.img" skip=573441 count=524289
