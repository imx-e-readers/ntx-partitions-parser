#ifndef NTX_FW_LM3630_H
#define NTX_FW_LM3630_H

struct NTX_FW_LM3630FL_dualcolor_hdr {
	unsigned long dwTotalColors; // total color temperatures .
	unsigned char bDefaultC1_Current; // default power/current on ch1 FL of lm3630a .
	unsigned char bDefaultC2_Current; // default power/current on ch2 FL of lm3630a .
	unsigned char bDefaultFreq; // default frequency of FL .
	unsigned char bDefaultFlags; // default flags about lm3630a .
};

struct NTX_FW_LM3630FL_dualcolor_percent_tab {
	char szColorName[32]; //
	unsigned char bDefaultC1_Current; // default power/current on ch1 FL of lm3630a for this color .
	unsigned char bDefaultC2_Current; // default power/current on ch2 FL of lm3630a for this color .
	unsigned char bDefaultFreq; // default frequency of FL for this color .
	unsigned char bDefaultFlags; // default flags about lm3630a for this color .
	unsigned char bC1_BrightnessA[100]; // brightness percentage table of Ch1 .
	unsigned char bC1_CurrentA[100]; // power/current percentage table of Ch1 .
	unsigned char bC2_BrightnessA[100]; // brightness percentage table of Ch2 .
	unsigned char bC2_CurrentA[100]; // power/current percentage table of Ch2 .
	unsigned long dwCurrentA[100]; // current table in each combination .
	unsigned char bReservedA[8];
};

#endif /* NTX_FW_LM3630_H */
