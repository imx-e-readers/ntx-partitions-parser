/*
 * Copyright (C) 2019 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"

int main(int argc, char** argv)
{
	uint64_t buffer = 0xabcdef0012345678;
	char* string;
	get_hex_string(&string, (char*)(&buffer), sizeof(buffer));
	assert(	strncmp("ABCD EF00 1234 5678", string, sizeof("ABCD EF00 1234 5678")) == 0);
	dbg_printf("%s: string@%p: %s\n", __FUNCTION__, string, string);
	free(string);
	printf("OK\n");
}
